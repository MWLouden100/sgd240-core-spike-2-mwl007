# Spike Report

## SPIKE TITLE - Blueprint Basics

### Introduction

This spike report details how to perform basic blueprinting tasks in Unreal Engine 4, specifically how to create an
adjustable launchpad for the player character and using it in a custom environment.

### Goals

1. Learn how to create and customise a blueprint class
2. Learn how to mainipulate the player character's velocity using a blueprint object
3. Learn how to implement adjustable variables for individual instances of blueprints

### Personnel

* Primary - Matthew Louden
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Unreal Engine Blueprints Visual Scripting Documentation] (https://docs.unrealengine.com/en-US/Engine/Blueprints/index.html)
 *[Unreal Engine Quick Start Guide] (https://docs.unrealengine.com/en-US/Engine/Blueprints/QuickStart/index.html)
 *[Unreal Engine Blueprint Class Creation Guide] (https://docs.unrealengine.com/en-US/Gameplay/ClassCreation/index.html)
* [Unreal Engine Forums - Launchpad Discussion] (https://answers.unrealengine.com/questions/349817/how-to-get-rotation-of-arrow-component.html#)
* [Unreal Engine Blueprint Tutorial] (https://www.raywenderlich.com/663-unreal-engine-4-blueprints-tutorial)

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Generated New UE4 Project worldspace (1st person)
	1. Created new [blueprint class actor] (https://docs.unrealengine.com/en-US/Gameplay/ClassCreation/index.html)
	2. Added a box collider to the blueprint actor
	3. added a box mesh to the blueprint actor
	4. added a directional arrow to the blueprint actor
		1a. added 'On Component Begin Overlap'
		1b. added 'Get Player Character' and used the '=' function to equate it to the 'other actor pin' of 'On Component Begin Overlap'
		1c. added a branch then added 'Play Sound at Location' to the branch
		1d. 'Cast To FirstPersonCharacter' from 'Play Sound at Location' then connected the return value node of 'Get Player Character' to the object node of 'Cast To FirstPersonCharacter'
		1e. added 'Launch Character' from 'Cast To FirstpersonCharacter'
		1f. got the forward vector from the Arrow actor and multiplied it by the launch power variable (new integer variable) and plugged it into the launch velocity node of 'Launch Character'


### What we found out

We learnt how to create custom blueprint classes and manipulate a player character's velocity using said blueprints.
Personally, I learnt how link a launch vector to an adjustable object within a blueprint actor.

